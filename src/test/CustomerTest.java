import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by D067833 on 04.05.2018.
 */

class CustomerTest {

    @Test
    public void testGetCustomerName() {
        String testName = "Bernd";
        assertEquals(testName, new Customer(testName).getName());
    }

    @Test
    public void testEmtyRentalContent() {
        Customer customer = new Customer("");
        String emptyResult = "Rental Record for \n\tTitle\t\tDays\tAmount\nAmount owed is 0.0\nYou earned 0 frequent renter points";
        assertEquals(emptyResult, customer.statement());
    }

    @Test
    public void testEmptyRentalLines() {
        Customer customer = new Customer("");
        assertEquals(4, customer.statement().split("\n").length);
    }

    @Test
    public void testEmptyRentalLinesContainingOne() {
        Customer customer = new Customer("");
        customer.addRental(new Rental(new Movie("", 1), 10));
        assertEquals(5, customer.statement().split("\n").length);
    }


}